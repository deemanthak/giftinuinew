webpackJsonp([12],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CompViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompViewPage = (function () {
    function CompViewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CompViewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompViewPage');
    };
    CompViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-comp-view',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/comp-view/comp-view.html"*/'<!--\n  Generated template for the CompViewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>compView</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="comp-view-container">\n  <div class="content-container">\n\n\n    <div class="image-container">\n      <img src="https://www.lifeloveandsugar.com/wp-content/uploads/2014/04/Perfect_Moist_Fluffy_Vanilla_Cupcakes.jpg">\n    </div>\n\n    <div class="meta-container px-10">\n\n      <h3 class="text-giftin">The Quick Brown fox jumps</h3>\n      <p>Posted by\n        <b>\n          <span class="text-giftin">Company</span>\n        </b> on\n        <span class="text-giftin">February 11th</span>\n      </p>\n      <p class="dflex-ai-center text-giftin">\n        <ion-icon name="heart-outline" class="text-25 "></ion-icon>\n        <span class="ml-5"> 324 </span>\n      </p>\n      <p class="text-giftin">\n          <strong>Your entry No: </strong> <span> 45 </span>\n        </p>\n    </div>\n    <div class="info-container px-10">\n      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus quos corrupti alias laudantium, repellendus voluptatem\n        ut fugiat. Impedit tempora quas adipisci eum soluta molestias. Quos quo facilis blanditiis voluptatem? Eveniet.</p>\n      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus quos corrupti alias laudantium, repellendus voluptatem\n        ut fugiat. Impedit tempora quas adipisci eum soluta molestias. Quos quo facilis blanditiis voluptatem? Eveniet.</p>\n      <hr>\n      <div class="link-container">\n        <button clear color="light" ion-fab class="link-icons">\n          <i class="fa fa-globe"></i>\n        </button>\n        <button clear color="light" ion-fab class="link-icons">\n          <i class="fa fa-facebook-f"></i>\n        </button>\n        <button clear color="light" ion-fab class="link-icons">\n          <i class="fa fa-twitter"></i>\n        </button>\n        <button clear color="light" ion-fab class="link-icons">\n          <i class="fa fa-youtube"></i>\n        </button>\n        <button clear color="light" ion-fab class="link-icons">\n          <i class="fa fa-instagram"></i>\n        </button>\n      </div>\n    </div>\n  </div>\n  <div class="draw-container px-0 mt-20">\n    <h6 class="px-10 text-16 text-giftin"> Draw Details</h6>\n    <ion-card>\n      <ion-card-content>\n        <div *ngFor="let lot of [1,2,3]" class="">\n          <p>Lottery {{lot}}</p>\n          <p class="lotteries text-giftin">\n            <span *ngFor="let num of [31,22,3,45,6,43,76]" class="lot-numbers">{{num}}</span>\n          </p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </div>\n  <div class="winner-container px-0 mt-20">\n    <h6 class="px-10 text-16 text-giftin"> Winners</h6>\n    <ion-card>\n      <ion-card-content>\n        <ion-slides pager="false" autoplay="1000" slidesPerView="4" pager="true">\n          <ion-slide  *ngFor="let winner of [1,2,3,4,5,6,7,5,3,2,2,2]">\n            <div class="winners">\n              <ion-avatar>\n                <img src="https://png.icons8.com/color/1600/circled-user-female-skin-type-1-2.png">\n              </ion-avatar>\n              <p class="text-elepsis">name {{winner}}</p>\n            </div>\n\n          </ion-slide>\n\n        </ion-slides>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/comp-view/comp-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], CompViewPage);
    return CompViewPage;
}());

//# sourceMappingURL=comp-view.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EntryPage = (function () {
    function EntryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EntryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EntryPage');
    };
    EntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/entry/entry.html"*/'<!--\n  Generated template for the EntryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>My Basket</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<div class="grid-full">\n  <ion-row>\n    <ion-col col-6 *ngFor="let item of [1,2,3,4,5,6,7,8,9,10]">\n        <ion-card>\n            \n          <ion-card-header class="text-giftin">\n            The Quick brown fox jumps\n            <p><small>by Company</small></p>\n          </ion-card-header>\n            <img class="card-image" src="https://t04.deviantart.net/92tU9m438acFPI5-atZUcpBWtgc=/300x200/filters:fixed_height(100,100):origin()/pre04/d404/th/pre/f/2011/248/e/e/little_merry_go_round_by_welcomehomejane-d48xrmp.jpg"/>\n            <ion-card-content>\n                <p class="dflex-ai-center">\n                  <span style="width:50%;">\n                      <ion-icon name="heart-outline" class=""></ion-icon>\n                      <span class="ml-5"> 324 </span>\n                  </span>\n                  <span  style="width:50%;">\n                    \n                    <!-- <ion-icon name="heart-outline" class=""></ion-icon> -->\n                    <span class="ml-5"> 67 </span>\n                  </span>\n                     \n                </p>\n            </ion-card-content>\n          </ion-card>\n    </ion-col>\n  </ion-row>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/entry/entry.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], EntryPage);
    return EntryPage;
}());

//# sourceMappingURL=entry.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FgtPassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FgtPassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FgtPassPage = (function () {
    function FgtPassPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FgtPassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FgtPassPage');
    };
    FgtPassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fgt-pass',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/fgt-pass/fgt-pass.html"*/'<!--\n  Generated template for the FgtPassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content padding>\n    <ion-grid >\n        <div>\n          <div text-center class="text-white">\n          <h4>Forgot Your Password?</h4>\n          <p>Enter your mobile number to reset your password</p>\n        </div>\n          <ion-row>\n            <ion-col>\n              <ion-list>\n                <form>\n                  <div class="item-container">\n                  <ion-item class="px-10">\n                    <ion-input type="number" placeholder="Contact Number" clearInput></ion-input>\n                  </ion-item>\n                  \n                </div>\n                  <ion-row>\n                    <ion-col class="px-0">\n                      <button type="submit" ion-button class="btn-gift" color="light" block>Submit</button>\n                    </ion-col>\n                  </ion-row>\n                \n                </form>\n                <p text-center class="text-white">Want to go back?\n                    <span><b>Sign in</b></span>\n                  </p>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/fgt-pass/fgt-pass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FgtPassPage);
    return FgtPassPage;
}());

//# sourceMappingURL=fgt-pass.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FullViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FullViewPage = (function () {
    function FullViewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FullViewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FullViewPage');
    };
    FullViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-full-view',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/full-view/full-view.html"*/'<!--\n  Generated template for the FullViewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n\n  <ion-navbar>\n    <!-- <ion-title>fullView</ion-title> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="full-view-content">\n  <div class="image-container">\n    <img src="https://www.lifeloveandsugar.com/wp-content/uploads/2014/04/Perfect_Moist_Fluffy_Vanilla_Cupcakes.jpg">\n  </div>\n\n  <div class="meta-container px-10">\n    <div class="grab-button-container">\n        <button ion-fab class="grab-button gift-button"><ion-icon name="basket"></ion-icon></button>\n    </div>\n    <h3 class="text-giftin">The Quick Brown fox jumps</h3>\n    <p>Posted by\n      <b>\n        <span class="text-giftin">Company</span>\n      </b> on\n      <span class="text-giftin">February 11th</span>\n    </p>\n    <p class="dflex-ai-center text-giftin">\n      <ion-icon name="heart-outline" class="text-25 "></ion-icon>\n      <span class="ml-5"> 324 </span>\n    </p>\n    <p class="text-giftin">\n      <strong>Your entry No: </strong> <span> 45 </span>\n    </p>\n  </div>\n  <div class="info-container px-10">\n    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus quos corrupti alias laudantium, repellendus voluptatem\n      ut fugiat. Impedit tempora quas adipisci eum soluta molestias. Quos quo facilis blanditiis voluptatem? Eveniet.</p>\n    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus quos corrupti alias laudantium, repellendus voluptatem\n      ut fugiat. Impedit tempora quas adipisci eum soluta molestias. Quos quo facilis blanditiis voluptatem? Eveniet.</p>\n<hr>\n<div class="link-container">\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-globe"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-facebook-f"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-twitter"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-youtube"></i></button>\n    <button clear color="light" ion-fab  class="link-icons" ><i class="fa fa-instagram"></i></button>\n</div>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/full-view/full-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], FullViewPage);
    return FullViewPage;
}());

//# sourceMappingURL=full-view.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__otp_otp__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fgt_pass_fgt_pass__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__reset_pwd_reset_pwd__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__profile_profile__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__edit_user_edit_user__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__full_view_full_view__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__entry_entry__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__comp_view_comp_view__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__winnings_winnings__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { ListPage } from '../list/list';











/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MenuPage = (function () {
    function MenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.homePage = __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */];
        this.activePage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        // this.childNavCtrl.push(ProfilePage);
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], icon: 'home' },
            { title: 'Sign in', component: __WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */], icon: 'person' },
            { title: 'Sign up', component: __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */], icon: 'person' },
            { title: 'Profile', component: __WEBPACK_IMPORTED_MODULE_8__profile_profile__["a" /* ProfilePage */], icon: 'person' },
            { title: 'Otp', component: __WEBPACK_IMPORTED_MODULE_5__otp_otp__["a" /* OtpPage */], icon: 'lock' },
            { title: 'edit', component: __WEBPACK_IMPORTED_MODULE_9__edit_user_edit_user__["a" /* EditUserPage */], icon: 'lock' },
            { title: 'Fgt pass', component: __WEBPACK_IMPORTED_MODULE_6__fgt_pass_fgt_pass__["a" /* FgtPassPage */], icon: 'lock' },
            { title: 'full view', component: __WEBPACK_IMPORTED_MODULE_10__full_view_full_view__["a" /* FullViewPage */], icon: 'lock' },
            { title: 'Entry', component: __WEBPACK_IMPORTED_MODULE_11__entry_entry__["a" /* EntryPage */], icon: 'lock' },
            { title: 'Reset pass', component: __WEBPACK_IMPORTED_MODULE_7__reset_pwd_reset_pwd__["a" /* ResetPwdPage */], icon: 'lock' },
            { title: 'Winning', component: __WEBPACK_IMPORTED_MODULE_13__winnings_winnings__["a" /* WinningsPage */], icon: 'trophy' },
            { title: 'Completed', component: __WEBPACK_IMPORTED_MODULE_12__comp_view_comp_view__["a" /* CompViewPage */], icon: 'check' }
        ];
        this.activePage = this.pages[0];
        // this.openPageS("home");
        // this.openPage(this.pages[8]);
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage.prototype.checkActive = function (page) {
        return page == this.activePage;
    };
    MenuPage.prototype.openPage = function (page) {
        console.log(page);
        // this.childNavCtrl.push(page.component);
        // this.childNavCtrl.push(page.component);
        this.childNavCtrl.setRoot(page.component);
        this.activePage = page;
    };
    MenuPage.prototype.openPageS = function (pageName) {
        // console.log("ac pae",this.navCtrl.getActive().component);
        switch (pageName) {
            case "home":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                break;
            case "profile":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__profile_profile__["a" /* ProfilePage */]);
                break;
            case "login":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
                break;
            case "signup":
                this.childNavCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */])
    ], MenuPage.prototype, "childNavCtrl", void 0);
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/menu/menu.html"*/'<ion-menu type="push" persistent="true" swipeEnabled="true" [content]="content">\n  <!-- <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header> -->\n\n  <ion-content class="menu-content">\n\n    <div class="user-info-container">\n      <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg">\n     <div class="mt-m-50">\n        <h4>John Doe</h4>\n              <p>New York, NY</p>\n     </div>\n    </div>\n    \n\n    <ion-list class="menu-list mt-10" no-lines>\n      <!-- <ion-item-divider color="danger">Main App</ion-item-divider> -->\n      <ion-item class="menu-item" text-wrap *ngFor="let p of pages" menuClose  (click)="openPage(p)" [class.activehighlight]="checkActive(p)" >\n        <ion-icon [name]="p.icon" item-left large></ion-icon>\n        <h2> {{p.title}} </h2>\n      </ion-item>\n\n     \n\n      <!-- <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'about\')">\n        <ion-icon name="planet" item-left large></ion-icon>\n        <h2> About </h2>\n      </ion-item> -->\n\n      <!-- <ion-item-divider color="danger">Profile</ion-item-divider> -->\n      <!-- <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'login\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> Login </h2>\n      </ion-item>\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'signup\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> Sign Up </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'profile\')">\n        <ion-icon name="contact" item-left large></ion-icon>\n        <h2> My Profile </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click) = "openPage(\'entries\')">\n        <ion-icon name="checkmark-circle" item-left large></ion-icon>\n        <h2> Entries </h2>\n      </ion-item>\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose (click)="openPage(\'winnings\')">\n        <ion-icon name="trophy" item-left large></ion-icon>\n        <h2> Winnings </h2>\n      </ion-item> -->\n\n      <ion-item color="light"  class="menu-item" text-wrap  menuClose>\n        <ion-icon name="trophy" item-left large></ion-icon>\n        <h2> Logout </h2>\n      </ion-item>\n\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav #content [root]= "homePage"></ion-nav>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OtpPage = (function () {
    function OtpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OtpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OtpPage');
    };
    OtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-otp',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/otp/otp.html"*/'<!--\n  Generated template for the OtpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n\n  <ion-grid>\n    <div text-center class="text-white">\n      <h4>Verify Mobile Number</h4>\n      <p>OTP has been sent to your mobile number. Please enter it below</p>\n    </div>\n    <ion-list>\n\n      <div class="item-container">\n        <ion-item class="px-10">\n          <ion-input type="number" clearInput placeholder="OTP"></ion-input>\n        </ion-item>\n      </div>\n      <ion-row>\n          <ion-col class="px-0">\n            <button type="submit" ion-button class="btn-gift" color="light" block>Verify</button>\n          </ion-col>\n        </ion-row>\n    </ion-list>\n    \n    <p text-center class="text-white">Didn\'t receive Verification Code?\n      <span>\n        <b>Resend</b>\n      </span>\n    </p>\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/otp/otp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], OtpPage);
    return OtpPage;
}());

//# sourceMappingURL=otp.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPwdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ResetPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetPwdPage = (function () {
    function ResetPwdPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ResetPwdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetPwdPage');
    };
    ResetPwdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reset-pwd',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/reset-pwd/reset-pwd.html"*/'<!--\n  Generated template for the ResetPwdPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n    <ion-grid >\n        <div>\n          <div text-center class="text-white">\n          <h4>Reset Your Password</h4>\n          <p>Enter your new password</p>\n        </div>\n          <ion-row>\n            <ion-col>\n              <ion-list>\n                <form>\n                  <div class="item-container">\n                  <ion-item class="px-10">\n                    <ion-input type="password" placeholder="New Password" clearInput></ion-input>\n                  </ion-item>\n                  <ion-item class="px-10">\n                      <ion-input type="password" placeholder="Confirm Password" clearInput></ion-input>\n                    </ion-item>\n                  \n                </div>\n                  <ion-row>\n                    <ion-col class="px-0">\n                      <button type="submit" ion-button class="btn-gift" color="light" block>Submit</button>\n                    </ion-col>\n                  </ion-row>\n                \n                </form>\n                <p text-center class="text-white">Want to go back?\n                    <span><b>Sign in</b></span>\n                  </p>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/reset-pwd/reset-pwd.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ResetPwdPage);
    return ResetPwdPage;
}());

//# sourceMappingURL=reset-pwd.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_user_edit_user__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profile = "General";
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.gotoEditPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__edit_user_edit_user__["a" /* EditUserPage */]);
        console.log("Edit page");
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/profile/profile.html"*/'<ion-header no-border>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>My Profile</ion-title>\n    <ion-buttons end (click)="gotoEditPage()">\n        <button ion-button icon-only>\n          <ion-icon name="create"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="user-info-container">\n    <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg">\n    <div class="mt-m-50">\n      <h4>John Doe</h4>\n      <p>New York, NY</p>\n    </div>\n  </div>\n  <div class="profile-container">\n\n\n    <ion-row class="mt-40">\n      <ion-col class="px-0">\n        <ion-card class="about-info">\n\n          <ion-card-header>\n            <h4>About</h4>\n          </ion-card-header>\n\n          <ion-card-content>\n            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. </p>\n            <hr>\n            <div class="user-stats">\n              <ion-row>\n                <ion-col id="l1">\n                  <h4>34</h4>\n                  <p>Likes</p>\n                </ion-col>\n                <ion-col id="l2">\n                  <h4>10</h4>\n                  <p>Wins</p>\n                </ion-col>\n                <ion-col id="l3">\n                  <h4>65</h4>\n                  <p>Interests</p>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class="mt-20">\n      <ion-col class="px-0">\n        <ion-card class="general-container">\n          <ion-card-header>\n            <h4>Basic Info</h4>\n          </ion-card-header>\n          <ion-card-content>\n\n            <hr>\n            <div class="user-info">\n              <ion-item>\n                <p>\n                  <b>Age</b>\n                </p>\n                <p>24</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Email</b>\n                </p>\n                <p>johndoe@abc.com</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Contact No</b>\n                </p>\n                <p>+94 71 234 5678</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>City</b>\n                </p>\n                <p>Colombo</p>\n              </ion-item>\n              <ion-item>\n                <p>\n                  <b>Country</b>\n                </p>\n                <p>Sri Lanka</p>\n              </ion-item>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WinningsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the WinningsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WinningsPage = (function () {
    function WinningsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    WinningsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WinningsPage');
    };
    WinningsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-winnings',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/winnings/winnings.html"*/'<!--\n  Generated template for the WinningsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>winnings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list no-lines class="px-10">\n    <ion-item class="winning-items" *ngFor="let item of [1,2,3,4,5,6,7,8,9,9,8,7,6,5,4]">\n      <ion-thumbnail item-start>\n        <img src="https://t04.deviantart.net/92tU9m438acFPI5-atZUcpBWtgc=/300x200/filters:fixed_height(100,100):origin()/pre04/d404/th/pre/f/2011/248/e/e/little_merry_go_round_by_welcomehomejane-d48xrmp.jpg">\n      </ion-thumbnail>\n      <div class="text-giftin">\n          <h4 class="text-16 text-elepsis" >My Neighbor Totoro</h4>\n          <p><small>by Company item</small></p>\n      </div>\n      \n      <button ion-button outline item-end class="btn-gift-outline">View</button>\n    </ion-item>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/winnings/winnings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], WinningsPage);
    return WinningsPage;
}());

//# sourceMappingURL=winnings.js.map

/***/ }),

/***/ 121:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 121;

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/comp-view/comp-view.module": [
		284,
		11
	],
	"../pages/edit-user/edit-user.module": [
		285,
		10
	],
	"../pages/entry/entry.module": [
		286,
		9
	],
	"../pages/fgt-pass/fgt-pass.module": [
		287,
		8
	],
	"../pages/full-view/full-view.module": [
		288,
		7
	],
	"../pages/login/login.module": [
		290,
		6
	],
	"../pages/menu/menu.module": [
		289,
		5
	],
	"../pages/otp/otp.module": [
		291,
		4
	],
	"../pages/profile/profile.module": [
		292,
		3
	],
	"../pages/reset-pwd/reset-pwd.module": [
		293,
		2
	],
	"../pages/signup/signup.module": [
		294,
		1
	],
	"../pages/winnings/winnings.module": [
		295,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 162;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = (function () {
    function HomePage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.last_slide = false;
        this.className = "";
        this.clStatus = false;
        this.btnStatus = false;
        this.data = {};
        this.data.username = 'admin';
        this.data.password = 'Admin123$';
        this.data.response = '';
    }
    HomePage.prototype.submit = function () {
    };
    HomePage.prototype.presentPopover = function (myEvent) {
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.slideChanged = function () {
        this.currentIndex = this.slides.getActiveIndex();
        // this.prevI = this.slides.getPreviousIndex();
        if (this.slides.ionSlideReachEnd) {
            this.last_slide = true;
            this.slides.lockSwipeToNext(true);
        }
        else {
            this.last_slide = false;
            this.slides.lockSwipeToNext(false);
        }
        if (this.slides.isEnd()) {
            this.last_slide = true;
            this.slides.lockSwipeToNext(true);
        }
        else {
            this.last_slide = false;
            this.slides.lockSwipeToNext(false);
        }
        // console.log('Current index is', currentIndex);
    };
    HomePage.prototype.ngAfterViewInit = function () {
        this.cards = [];
        this.addNewCards(10);
        console.log(this.cards);
    };
    //add new cards to the stack
    HomePage.prototype.addNewCards = function (count) {
        var no = Math.floor(Math.random() * 20) + 1 + Math.floor(Math.random() * 10);
        for (var i = 0; i < count; ++i) {
            this.cards.push("item " + Math.floor(Math.random() * 20) + 1 + Math.floor(Math.random() * 10));
        }
        this.test_text = no;
        if (no > 15) {
            return true;
        }
        else {
            return false;
        }
    };
    HomePage.prototype.endReached = function () {
        this.last_slide = true;
    };
    //get the index of the card and splice them.
    HomePage.prototype.voteUp = function (id) {
        var _this = this;
        this.clStatus = true;
        // const removedCard = this.cards.pop();
        this.btnStatus = false;
        setTimeout(function () {
            _this.btnStatus = true;
            _this.clStatus = false;
            _this.liked_no = id;
            _this.liked_val = _this.cards[id];
            _this.cards.splice(id, 1);
            _this.slides.update(0);
            _this.slides.resize();
            // let added = this.addNewCards(1);
            var added = false;
            _this.test_text = added;
            if (added) {
                if (_this.last_slide) {
                    _this.slides.lockSwipeToNext(true);
                    _this.prevI = _this.slides.getActiveIndex() - 1;
                    _this.slides.slideTo(_this.prevI, 1000);
                }
                else {
                    _this.slides.lockSwipeToNext(false);
                    // this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
                }
            }
            else {
                if (_this.last_slide) {
                    _this.slides.lockSwipeToNext(true);
                    _this.prevI = _this.slides.getActiveIndex() - 1;
                    _this.slides.slideTo(_this.prevI, 1000);
                }
                else {
                    _this.slides.lockSwipeToNext(false);
                    //  this.slides.slideTo(this.slides.getActiveIndex(),1000);
                    // this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
                }
            }
            // if(this.slides.ionSlideReachEnd){
            // 	this.slides.lockSwipeToNext(true);
            // }else{
            // 	this.slides.lockSwipeToNext(false);
            // }
            _this.slides.update(0);
            _this.slides.resize();
            console.log("POPPED");
            console.log(_this.slides.getActiveIndex());
            if (_this.slides.length() <= 0) {
                _this.test_text = "no more slides";
            }
            // this.addNewCards(1);
        }, 1000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */])
    ], HomePage.prototype, "slides", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/home/home.html"*/'<!--\n  Generated template for the HomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n\n  <ion-navbar>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>home</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding="">\n    <div class="entry-no-back" [ngClass]="clStatus ? \'\' : \'show-no\' ">\n        <h2 class="entry-no">5</h2>\n      </div>\n\n      \n    <ion-slides let slides pager="true" paginationType="bullets" centeredSlides="true" spaceBetween="25" (ionSlideDidChange)="slideChanged()" (ionSlideReachEnd) = "endReached()" >\n\n        <ion-slide width="10px" *ngFor="let item of cards" [ngClass]="clStatus ? \'element-animation\' : \'\' ">\n          <img src="https://t04.deviantart.net/92tU9m438acFPI5-atZUcpBWtgc=/300x200/filters:fixed_height(100,100):origin()/pre04/d404/th/pre/f/2011/248/e/e/little_merry_go_round_by_welcomehomejane-d48xrmp.jpg" class="slide-image"/>\n          <h2 class="slide-title">\n            Pick the <b>Gift {{ item }}</b>\n          </h2>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur asperiores id possimus nulla corrupti quasi alias facilis numquam. Debitis est ratione totam explicabo officia quae distinctio. Reiciendis quam nostrum at.\n            </p>\n          <!-- <button ion-fab (click)="voteUp(i)">{{ i }}</button> -->\n          <ion-row>\n              <ion-col style="position:absolute;\n              bottom: 0px;">\n                <button ion-button outline small center text-center class="btn-gift-outline">\n                  <div>View More</div>\n                </button>\n              </ion-col>\n            </ion-row>\n          \n        </ion-slide>\n    \n      </ion-slides>\n      \n      <ion-fab bottom center>\n        <button ion-fab (click)="voteUp(currentIndex)" class="gift-button" [ngClass]="btnStatus ? \'btn-animation\' : \'\' "> <ion-icon name="basket"></ion-icon></button>\n      </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* PopoverController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(231);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_otp_otp__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_fgt_pass_fgt_pass__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_reset_pwd_reset_pwd__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_menu_menu__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_edit_user_edit_user__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_full_view_full_view__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_entry_entry__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_winnings_winnings__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_comp_view_comp_view__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_otp_otp__["a" /* OtpPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_fgt_pass_fgt_pass__["a" /* FgtPassPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_reset_pwd_reset_pwd__["a" /* ResetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_edit_user_edit_user__["a" /* EditUserPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_full_view_full_view__["a" /* FullViewPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_winnings_winnings__["a" /* WinningsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_comp_view_comp_view__["a" /* CompViewPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: true,
                    autoFocusAssist: true
                }, {
                    links: [
                        { loadChildren: '../pages/comp-view/comp-view.module#CompViewPageModule', name: 'CompViewPage', segment: 'comp-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit-user/edit-user.module#EditUserPageModule', name: 'EditUserPage', segment: 'edit-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/entry/entry.module#EntryPageModule', name: 'EntryPage', segment: 'entry', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fgt-pass/fgt-pass.module#FgtPassPageModule', name: 'FgtPassPage', segment: 'fgt-pass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/full-view/full-view.module#FullViewPageModule', name: 'FullViewPage', segment: 'full-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/otp/otp.module#OtpPageModule', name: 'OtpPage', segment: 'otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reset-pwd/reset-pwd.module#ResetPwdPageModule', name: 'ResetPwdPage', segment: 'reset-pwd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/winnings/winnings.module#WinningsPageModule', name: 'WinningsPage', segment: 'winnings', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_otp_otp__["a" /* OtpPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_fgt_pass_fgt_pass__["a" /* FgtPassPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_reset_pwd_reset_pwd__["a" /* ResetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_edit_user_edit_user__["a" /* EditUserPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_full_view_full_view__["a" /* FullViewPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_winnings_winnings__["a" /* WinningsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_comp_view_comp_view__["a" /* CompViewPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_menu_menu__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_menu_menu__["a" /* MenuPage */];
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/app/app.html"*/'<!-- <ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<!-- <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>  -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditUserPage = (function () {
    function EditUserPage(navCtrl, navParams, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
    }
    EditUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditUserPage');
    };
    EditUserPage.prototype.presentActionSheet = function () {
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Edit Profile Photo',
            buttons: [
                {
                    text: 'Pick From Gallery',
                    icon: 'images',
                    // role: 'destructive',
                    handler: function () {
                        console.log('Destructive clicked');
                    }
                }, {
                    text: 'Take a Picture',
                    icon: 'camera',
                    handler: function () {
                        console.log('Archive clicked');
                    }
                }, {
                    text: 'Delete',
                    icon: 'trash',
                    // role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'close',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    EditUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-user',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/edit-user/edit-user.html"*/'<!--\n  Generated template for the EditUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header no-border>\n    <!-- <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button> -->\n  <ion-navbar>\n    <ion-title>Edit My Info</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="edit-user-content">\n    <div class="user-info-container">\n      <div class="img-container">\n          <img class="avatar" src="http://demos.creative-tim.com/material-kit/assets/img/kit/faces/christian.jpg">\n          <button ion-fab color="light" class="img-upload-btn-fab" (click)="presentActionSheet()"><ion-icon name="camera"></ion-icon></button>\n          <!-- <button class="" ion-button color="light" icon-only>\n              <ion-icon name="camera"></ion-icon>\n            </button> -->\n      </div>\n      </div>\n\n  <div class="profile-container">\n\n  <ion-row class="mt-40">\n      <ion-col class="px-0">\n        <ion-card class="about-info">\n          <ion-card-content>\n              <div class="edit-form-container">\n                  <ion-item class="item-divider-header">\n                      <h5>Personal information</h5>\n                      <button ion-button  outline item-end class="btn-gift-outline">save</button>\n                  </ion-item>\n                  <hr class="mt-m-10">\n                  <div class="form-container">\n                      <ion-list >\n                          <form >\n                            <ion-row>\n                              <ion-col >\n                                <ion-item class="px-10">\n                                  <ion-label stacked>First Name</ion-label>\n                                  <ion-input type="text"></ion-input>\n                                </ion-item>\n                                </ion-col>\n                                <ion-col>\n                                <ion-item class="px-10">\n                                  <ion-label stacked>Last Name</ion-label>\n                                  <ion-input type="text"></ion-input>\n                                </ion-item>\n                              </ion-col>\n                            </ion-row>\n                            <ion-row>\n                              <ion-col>\n                                <ion-item class="px-10">\n                                  <ion-label stacked>Email</ion-label>\n                                  <ion-input type="text"></ion-input>\n                                </ion-item>\n                                <ion-item class="px-10">\n                                  <ion-label stacked>Contact No</ion-label>\n                                  <ion-input type="number"></ion-input>\n                                </ion-item>\n                                <ion-item class="px-10">\n                                    <ion-label stacked>City</ion-label>\n                                    <ion-input type="text"></ion-input>\n                                  </ion-item>\n                                  <ion-item class="px-10">\n                                    <ion-label stacked>Country</ion-label>\n                                    <ion-input type="text"></ion-input>\n                                  </ion-item>\n                              </ion-col>\n                    \n                            </ion-row>\n                    \n                          </form>\n                    \n                        </ion-list>\n                        \n                  </div>  \n                  \n                  <ion-item class="item-divider-header">\n                  <h5>Reset Password</h5>\n                      <button ion-button  outline item-end class="btn-gift-outline">Submit</button>\n                  </ion-item>\n                  <hr class="mt-m-10">\n                  <form>\n                      <ion-row>\n                          <ion-item class="px-10">\n                            <ion-label stacked>Old password</ion-label>\n                            <ion-input type="text"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                            <ion-label stacked>New Password</ion-label>\n                            <ion-input type="text"></ion-input>\n                          </ion-item>\n                          <ion-item class="px-10">\n                              <ion-label stacked>Retype new Password</ion-label>\n                              <ion-input type="text"></ion-input>\n                            </ion-item>\n                      </ion-row>\n                      \n              \n                    </form>\n                </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/edit-user/edit-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
    ], EditUserPage);
    return EditUserPage;
}());

//# sourceMappingURL=edit-user.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.signUp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding="" >\n  <ion-grid >\n    <div>\n      <div text-center class="text-white">\n      <h4>Sign In</h4>\n      <p>Please Sign In to continue</p>\n    </div>\n      <ion-row>\n        <ion-col>\n          <ion-list>\n            <form>\n              <div class="item-container">\n              <ion-item class="px-10">\n                <ion-input type="email" placeholder="Email" clearInput></ion-input>\n              </ion-item>\n              <ion-item class="px-10">\n                <ion-input type="password"  placeholder="Password" clearInput></ion-input>\n              </ion-item>\n            </div>\n              <ion-row>\n                <ion-col class="px-0">\n                  <button type="submit" ion-button class="btn-gift" color="light" block>Sign in</button>\n                  <!-- [disabled]="!user.valid" -->\n                </ion-col>\n              </ion-row>\n            \n            </form>\n            <p text-center class="text-white">Forgot Password?\n                <span><b>Reset Password</b></span>\n              </p>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n     \n      <ion-row>\n        <ion-col text-center class="px-0">\n          <button ion-button class="social-icon social-facebook">\n            <!-- <ion-icon name="logo-facebook"></ion-icon> -->\n            <img src="../assets/imgs/fb_login.png" alt="">\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col text-center class="text-white">\n\n          <p>Don\'t have account?\n            <span (click)="signUp()" ><b>Sign up now</b></span>\n          </p>\n          \n\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = (function () {
    function SignupPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.signIn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/signup/signup.html"*/'<!--\n  Generated template for the SignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content padding>\n    <ion-grid >\n        <div>\n          <div text-center class="text-white">\n          <h4>Sign Up</h4>\n          <p>Please Sign Up to continue</p>\n        </div>\n          <ion-row>\n            <ion-col>\n              <ion-list>\n                <form>\n                  <div class="item-container">\n                     \n                      <div class="has-error" >This field is required and should be valid email</div>\n                      <ion-item class="px-10">\n                      \n                    <ion-input type="email" placeholder="Email" clearInput></ion-input>\n                  </ion-item>\n                  <!-- <div class="has-error" >This field is required and should be valid email</div> -->\n                  <ion-item class="px-10">\n                      <ion-input required type="number" placeholder="Contact Number" clearInput></ion-input>\n                    </ion-item>\n                  <ion-item class="px-10">\n                    <ion-input type="password"  placeholder="Password" clearInput></ion-input>\n                  </ion-item>\n                </div>\n                  <ion-row>\n                    <ion-col class="px-0">\n                      <button type="submit" ion-button class="btn-gift" color="light" block>Sign Up</button>\n                      <!-- [disabled]="!user.valid" -->\n                    </ion-col>\n                  </ion-row>\n                </form>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n         \n          <ion-row>\n            <ion-col text-center class="px-0">\n              <button ion-button class="social-icon social-facebook">\n                <!-- <ion-icon name="logo-facebook"></ion-icon> -->\n                <img src="../assets/imgs/fb_login.png" alt="">\n              </button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col text-center class="text-white">\n    \n              <p>Already have account?\n                <span (click)="signIn()"><b>Sign in</b></span>\n              </p>\n              \n    \n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/MacBook/Documents/MJMobile/giftin-ui/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

},[207]);
//# sourceMappingURL=main.js.map