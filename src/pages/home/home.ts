import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams,Slides,PopoverController } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 @Component({
 	selector: 'page-home',
 	templateUrl: 'home.html',
 })
 export class HomePage {
 	base_url:any;
 	cards:Array<any>;
 	currentIndex: number;
 	prevI:number;
 	last_slide: boolean = false;

 	liked_no:number;
 	liked_val:any;

 	className:string = "";
 	clStatus:boolean = false;

 	test_text:any;

   	btnStatus:boolean = false;

 	data:any = {};

 	@ViewChild(Slides) slides: Slides;

 	constructor(public navCtrl: NavController, public navParams: NavParams,
 		public popoverCtrl : PopoverController,
 		) {
 		this.data.username = 'admin';
 		this.data.password = 'Admin123$';
 		this.data.response = '';
 	}
 	submit() {
 	}

 	presentPopover(myEvent) {
 	
 	}

 	ionViewDidLoad() {
 		
 		console.log('ionViewDidLoad HomePage');
 	}

 	slideChanged() {
 		this.currentIndex = this.slides.getActiveIndex();
 		// this.prevI = this.slides.getPreviousIndex();

 		if(this.slides.ionSlideReachEnd){
 			this.last_slide = true;
 			this.slides.lockSwipeToNext(true);
 		}else{
 			this.last_slide = false;
 			this.slides.lockSwipeToNext(false);
 		}

 		if(this.slides.isEnd()){
 			this.last_slide = true;
 			this.slides.lockSwipeToNext(true);
 		}else{
 			this.last_slide = false;
 			this.slides.lockSwipeToNext(false);
 		}
 		// console.log('Current index is', currentIndex);

 	}

 	ngAfterViewInit(){
 		this.cards = [];
 		this.addNewCards(10);
 		console.log(this.cards);
 	}

	//add new cards to the stack
	addNewCards(count: number) {
		let no = Math.floor(Math.random()*20)+1+Math.floor(Math.random()*10);
		for (var i = 0; i < count; ++i) {

			this.cards.push("item " + Math.floor(Math.random()*20)+1+Math.floor(Math.random()*10));
		}
		this.test_text = no;

		if(no > 15){
			return true;
		}else{
			return false;
		}
	}

	endReached(){
		this.last_slide = true;
	}

	
 	//get the index of the card and splice them.
 	voteUp(id:number) {
		this.clStatus=true;
     // const removedCard = this.cards.pop();
     this.btnStatus = false;
 		setTimeout(()=>{
			
      this.btnStatus = true;
 			this.clStatus=false;
 			this.liked_no = id;
 			this.liked_val = this.cards[id];
 			this.cards.splice(id,1);

 			this.slides.update(0);
 			this.slides.resize();

 			// let added = this.addNewCards(1);
 			let added = false;
 			this.test_text = added;

 			if(added){
 				if(this.last_slide){
 					this.slides.lockSwipeToNext(true);
 					this.prevI = this.slides.getActiveIndex() - 1;
 					this.slides.slideTo(this.prevI,1000);
 				}else{
 					this.slides.lockSwipeToNext(false);
 					// this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
 				}
 			}else{

 				if(this.last_slide){
 					this.slides.lockSwipeToNext(true);
 					this.prevI = this.slides.getActiveIndex() - 1;
 					this.slides.slideTo(this.prevI,1000);
 				}else{
					 this.slides.lockSwipeToNext(false);
					//  this.slides.slideTo(this.slides.getActiveIndex(),1000);
 				// this.slides.slideTo(this.slides.getActiveIndex() + 1,1000);
 			}
 		}
 		// if(this.slides.ionSlideReachEnd){
 		// 	this.slides.lockSwipeToNext(true);
 		// }else{
 		// 	this.slides.lockSwipeToNext(false);
 		// }

 		this.slides.update(0);
 		this.slides.resize()
 		
 		console.log("POPPED");
 		console.log(this.slides.getActiveIndex());
 		if(this.slides.length() <= 0 ){
 			this.test_text = "no more slides";
 		}
		 // this.addNewCards(1);
 	
		 
   },1000);
   
 	}

 }
