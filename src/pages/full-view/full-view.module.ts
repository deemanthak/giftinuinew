import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FullViewPage } from './full-view';

@NgModule({
  declarations: [
    FullViewPage,
  ],
  imports: [
    IonicPageModule.forChild(FullViewPage),
  ],
})
export class FullViewPageModule {}
