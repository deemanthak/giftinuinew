import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
// import { ListPage } from '../list/list';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { OtpPage } from '../otp/otp';
import { FgtPassPage } from '../fgt-pass/fgt-pass';
import { ResetPwdPage } from '../reset-pwd/reset-pwd';
import { ProfilePage } from '../profile/profile';
import { EditUserPage } from '../edit-user/edit-user';
import { FullViewPage } from '../full-view/full-view';
import { EntryPage } from '../entry/entry';
import {CompViewPage } from '../comp-view/comp-view';
import { WinningsPage } from '../winnings/winnings';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  homePage: any;
  activePage: any;

  pages: any;

  // @ViewChild('content') childNavCtrl: NavController;

@ViewChild('content') childNavCtrl:NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.homePage = SignupPage;
    this.activePage = HomePage;
    // this.childNavCtrl.push(ProfilePage);
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Sign in', component: LoginPage, icon: 'person' },
      { title: 'Sign up', component: SignupPage, icon: 'person' },
      { title: 'Profile', component: ProfilePage, icon: 'person' },
      { title: 'Otp', component: OtpPage, icon: 'lock' },
      { title: 'edit', component: EditUserPage, icon: 'lock' },
      { title: 'Fgt pass', component: FgtPassPage, icon: 'lock' },
      { title: 'full view', component: FullViewPage, icon: 'lock' },
      { title: 'Entry', component: EntryPage, icon: 'lock' },
      { title: 'Reset pass', component: ResetPwdPage, icon: 'lock' },
      { title: 'Winning', component: WinningsPage, icon: 'trophy' },
      {title: 'Completed',component:CompViewPage,icon:'check'}
    ]

    this.activePage = this.pages[0];

    // this.openPageS("home");
    // this.openPage(this.pages[8]);
  }

  ionViewDidLoad() {


    console.log('ionViewDidLoad MenuPage');
  }

  checkActive(page) {
    return page == this.activePage;
  }

  openPage(page) {
    console.log(page);
    // this.childNavCtrl.push(page.component);
    // this.childNavCtrl.push(page.component);
    this.childNavCtrl.setRoot(page.component);
    this.activePage = page;
  }

  openPageS(pageName: string) {
    // console.log("ac pae",this.navCtrl.getActive().component);
    switch (pageName) {
      case "home":
        this.childNavCtrl.setRoot(HomePage);
        break;
      case "profile":
        this.childNavCtrl.setRoot(ProfilePage);
        break;
      case "login":
        this.childNavCtrl.setRoot(LoginPage);
        break;
      case "signup":
        this.childNavCtrl.setRoot(SignupPage);
        break;
      // case "entries":
      // this.navCtrl.push(EntriesPage);
      // break;
      // case "winnings":
      // this.navCtrl.push(WinningsPage);
      // break;
      // case "about":
      // this.navCtrl.push(AboutPage);


    }
  }


}
