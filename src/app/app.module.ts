import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { OtpPage} from '../pages/otp/otp';
import { FgtPassPage } from '../pages/fgt-pass/fgt-pass';
import { ResetPwdPage } from '../pages/reset-pwd/reset-pwd';
import { ProfilePage } from '../pages/profile/profile';
import  { MenuPage } from '../pages/menu/menu';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { FullViewPage } from '../pages/full-view/full-view';
import { EntryPage } from '../pages/entry/entry';
import { WinningsPage } from '../pages/winnings/winnings';
import {CompViewPage } from '../pages/comp-view/comp-view';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    OtpPage,
    FgtPassPage,
    ResetPwdPage,
    ProfilePage,
    MenuPage,
    EditUserPage,
    FullViewPage,
    EntryPage,
    WinningsPage,
    CompViewPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      scrollPadding: false,
            scrollAssist: true, 
            autoFocusAssist: true
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    SignupPage,
    OtpPage,
    FgtPassPage,
    ResetPwdPage,
    ProfilePage,
    MenuPage,
    EditUserPage,
    FullViewPage,
    EntryPage,
    WinningsPage,
    CompViewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
