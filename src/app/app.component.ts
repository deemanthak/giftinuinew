import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import {OtpPage} from '../pages/otp/otp';
import { FgtPassPage} from '../pages/fgt-pass/fgt-pass';
import { ResetPwdPage } from '../pages/reset-pwd/reset-pwd';
import { ProfilePage } from '../pages/profile/profile';
import  { MenuPage } from '../pages/menu/menu';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { FullViewPage } from '../pages/full-view/full-view';
import { EntryPage } from '../pages/entry/entry';
import { WinningsPage } from '../pages/winnings/winnings';
import {CompViewPage } from '../pages/comp-view/comp-view';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = MenuPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // openPage(page) {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.nav.setRoot(page.component);
  // }
}
